# Certification

The provision by an independent body of written assurance that the [Participants](#participant) and [Resources](#resource) in question meet specific requirements.

## references
- Adapted from ISO: <https://www.iso.org/certification.html>
