# Interconnection & Networking Service 
Networking Service – services offered beyond the basic network functions, for example NTP, DNS, etc.

Interconnection Service – is the subclass of network service that runs on top of physical or logical interconnection. These can include best-effort connectivity and also go beyond it ensuring guaranteed bandwidths, lower latency, reliability and elevated security.

Interconnection & Networking service is a service that combines one or multiple services defined above.
