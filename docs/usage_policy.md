# Usage Policy

A Usage Policy is a [Policy in a technical sense](#policy-technical), by which a [Provider](#provider) constraints the [Consumer's](#consumer) use of the [Resources](#resource) offered.

## alias
- Provider Policy

## references
- according to IDSA: Usage Control in the IDS, IDS RAM 3.0
