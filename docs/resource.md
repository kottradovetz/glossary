# Resource

A Resource is an internal building block, not available for order, used to compose [Service Offerings](#service-offering).   

Resource classes include Physical Resource, Virtual Resource and an Instantiated Virtual Resource as defined below:

- A Physical Resource is a resource, that have a weight and position in our space such as, and not limited to, a data center, a bare metal service, a warehouse, a plant.
- A Virtual Resource is a resource describing recorded information such as, and not limited to, a dataset, a software, a configuration file, an AI model.
- An Instantiated Virtual Resource is a running software exposing endpoints such as, and not limited to, a running process, an online API, a network connection or interconnection, a virtual machine, a container, an operating system.

The resource attributes are detailed in the Gaia-X Trust Framework.
