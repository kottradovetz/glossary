# Ecosystem

An Ecosystem is an independant group of Participants that directly or indirectly consume, produce, or provide services such as data, storage, computing, network services, including combinations of them.

Technically speaking, there is no definition of a Gaia-X ecosystem, since the Gaia-X Compliance is applicable to Participants, Service Offering and related entities only.
However, it is commonly understood that a such an ecosystem would refer to a group of Gaia-X Compliant Participants exchanging Gaia-X Compliant services.

