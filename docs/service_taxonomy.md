# Service Taxonomy

Service Taxonomy is the class hierarchy of Gaia-X Conceptual Model that combines top-level classes such as [Praticipant](participant.md), [Service Offering](service_offering.md) and [Resource](resource.md).

