# Data Agreement Service

Data Agreement Service is a Federation Service of the category Data Sovereignty Service and considers negotiation of agreements for data exchange.

## alias
- DAS


## references
- Federation Services Specification GXFS
