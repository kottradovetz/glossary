# Compliance (Federation Service)

Compliance is a [Gaia-X Federation Service](federation_service.md).

It provides mechanisms to ensure that [Participants](#participant) and [Service Offerings](#service-offering) in a Gaia-X Ecosystem comply with the Compliance framework defined by Gaia-X, e.g., in the Policy Rules.
