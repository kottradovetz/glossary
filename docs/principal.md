# Principal

A Principal is an entity which is a member of a [Participant](#participant).
A Principal can be either a natural person or a digital representation of a Participant’s Resource.
