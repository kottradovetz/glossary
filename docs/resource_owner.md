# Resource Owner

A natural or legal person who is in legal possession of the [Resource](#resource) and is responsible to set policy rules on the [Resource](#resource).

Most Cloud Service Providers will be `Participants` with two roles: `Resource Owners` and `Providers`.
