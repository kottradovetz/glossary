# DSBA

## definition
Data Spaces Business Alliance (DSBA) consists of four European Associations locate in different EU member countries:
- Data, AI and Robotics (DAIRO) AISBL, https://www.bdva.eu/
- FIWARE Foundation, e.V., https://www.fiware.org/
- Gaia-X European Association for Data and Cloud AISBL, https://www.gaia-x.eu/
- International Data Spaces e.V., https://internationaldataspaces.org/

## references
- <https://www.gaia-x.eu/news/bdva-fiware-gaia-x-and-idsa-launch-alliance-accelerate-business-transformation-data-economy>
