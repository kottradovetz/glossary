# Endpoint

Combination of a binding and a network address.

## reference
- <https://www.iso.org/obp/ui/#iso:std:iso-iec:tr:23188:ed-1:v1:en:term:3.1.7>
