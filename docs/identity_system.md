# Identity System

An Identity System authenticates/provides additional attributes to the identity of the Gaia-X [Principal](#principal) and forwards this identity to the requestor.

A Gaia-X accredited Identity System follows a hybrid approach and consists of both centralized components, like company identity management systems, and decentralized components like Decentralized Identifiers (DIDs).
