# Participant

A Participant is an [Legal Person](legal_person.md) or [Natural Person](natural_person.md), which is identified, onboarded and has a Gaia-X Self-Description.

A Participant can take on one or multiple of the following roles: [Provider](#provider), [Consumer](#consumer), [Federator](#federator).
