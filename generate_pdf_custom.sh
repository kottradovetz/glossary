#!/bin/sh
# set -ex

# I. merge all md in docs in temp.md
mdArray=docs/*.md

for file in ${mdArray[@]}; do
  if [[ $file = "docs/README.md" ]]; then
  continue
  fi

  cat $file >> docs/temp.md
  echo " " >> docs/temp.md
done

# II. Create Table of Content and glossary.md. Merge ToC and temp.md in glossary.md.
toc=$(bash create_TOC.sh docs/temp.md)
echo "$toc" >> docs/glossary.md
echo " " >> docs/glossary.md
cat docs/temp.md >> docs/glossary.md 


# III. Add First letters for rendering pages by alphabetical order
GLOSSARY=docs/glossary.md 

HEADING_REGEX='^#{1,}'
LINE_COUNT=0
LETTER="0"

while read -r LINE; do
    LINE_COUNT=$(( ${LINE_COUNT} + 1 ))
    if [[ "${LINE}" =~ ${HEADING_REGEX} ]]; then
        # If we see first or second level heading, we save it to ToC map
        hash_count=$(echo "$LINE" | grep -o "#" | wc -l)

        if [[ ${hash_count} = 1 ]]; then
            firstCharacter=${LINE:2:1} # Get first letter of h1 title

            if [[ ${firstCharacter} != ${LETTER} ]]; then
            sed -i "$LINE_COUNT i ##### ${firstCharacter}\n" $GLOSSARY
            LETTER=$firstCharacter
            LINE_COUNT=$(( ${LINE_COUNT} + 2 ))
            fi
        fi
    fi    
done < $GLOSSARY


# IV Remove characters and render mermaid graph
mkdir output
bash convert_mermaid_graphs.sh docs/glossary.md
sed -i 's/“/"/' docs/glossary.md && sed -i 's/”/"/' docs/glossary.md


# V. Move glossary.md and png into temp
mkdir temp/
mv docs/glossary.md temp/
mv output/*.png temp/
